from flask import Flask

app = Flask(__name__)

from . import config, db, routes

db.db.create_all()