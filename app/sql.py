from hashlib import sha256
from uuid import uuid4

from sqlalchemy.dialects.postgresql import UUID as pgUUID

from .db import db


class Store(db.Model):
    id = db.Column(pgUUID(as_uuid=True), primary_key=True, default=uuid4)
    lock = db.Column(db.Boolean, default=False)
    key = db.Column(pgUUID(as_uuid=True), nullable=True)
    dbase = db.Column(db.Binary, default=b"")
    log = db.Column(db.Binary, default=b"")
    imgs = db.Column(db.Binary, default=b"")
    intake = db.Column(db.Binary, default=b"")
    h_dbase = db.Column(db.String, default=sha256(b"").digest().hex())
    h_log = db.Column(db.String, default=sha256(b"").digest().hex())
    h_imgs = db.Column(db.String, default=sha256(b"").digest().hex())
    h_intake = db.Column(db.String, default=sha256(b"").digest().hex())