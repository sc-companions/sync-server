from hashlib import sha256
from base64 import b64encode

from uuid import UUID, uuid4

from flask import jsonify, request, Request, abort

from . import app
from .db import db
from .sql import Store


@app.route("/create_storage")
def _create_session():
    storage = Store()
    db.session.add(storage)
    db.session.commit()
    db.session.flush()
    return str(storage.id)


@app.route("/<string:id>/lock", methods=["PUT"])
def _lock(id):
    rid = UUID(id)
    row = Store.query.get(rid)
    if not row.lock:
        row.lock = True
        row.key = uuid4()
        db.session.commit()
        return jsonify({"success": True, "key": row.key})
    return jsonify({"success": False})


@app.route("/<string:id>/lock", methods=["DELETE"])
def _release(id):
    rid = UUID(id)
    rq:Request = request
    row = Store.query.get(rid)
    if row.lock and UUID(rq.authorization.password) == row.key:
        row.lock = False
        row.key = None
        db.session.commit()
        return jsonify(True)
    return jsonify(False)


@app.route("/<string:id>/<string:asset>")
def _get_asset(id, asset):
    rid = UUID(id)
    rq:Request = request
    row = Store.query.get(rid)
    if row.lock and UUID(rq.authorization.password) == row.key:
        if rq.get_json()["hash"] != getattr(row, f"h_{asset}"):
            print("Got:", rq.get_json()["hash"])
            print("Have:", getattr(row, f"h_{asset}"))
            return jsonify({
                "modified": True,
                "content": b64encode(getattr(row, asset)).decode("utf-8")
            })
        else:
            return jsonify({
                "modified": False
            })
    abort(400)

  
@app.route("/<string:id>/<string:asset>", methods=["PUT"])
def _put_dbase(id, asset):
    rid = UUID(id)
    rq:Request = request
    row = Store.query.get(rid)
    if row.lock and UUID(rq.authorization.password) == row.key:
        setattr(row, asset, rq.data)
        setattr(row, f"h_{asset}", sha256(getattr(row, asset)).digest().hex())
        db.session.commit()
        return jsonify({"success": True})
    abort(400)